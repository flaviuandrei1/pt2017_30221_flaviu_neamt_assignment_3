package model;
public class Product {
	private int idProduct;
	private String name;
	private int price;
	private int stock;

	public Product(int idProduct, String name,int price, int stock ) {
		super();
		this.idProduct = idProduct;
		this.name = name;
		this.price = price;
		this.stock = stock;
	}

	public Product(int price, int stock, String name) {
		super();
		this.name = name;
		this.price = price;
		this.stock = stock;
	}

	public int getId() {
		return idProduct;
	}

	public void setId(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Product [id=" + idProduct + ", price=" + price + ", stock=" + stock + ", name=" + name+ "]";
	}

}