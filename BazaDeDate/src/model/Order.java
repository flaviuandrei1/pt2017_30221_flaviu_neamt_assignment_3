package model;
public class Order {
	private int idOrder;
	private int idClient;
	private int idProduct;

	public Order(){}
	public Order(int idOrder, int idClient, int idProduct) {
		super();
		this.idOrder = idOrder;
		this.idClient = idClient;
		this.idProduct = idProduct;
		
	}

	public Order(int idClient, int idProduct) {
		super();
		this.idClient = idClient;
		this.idProduct = idProduct;
		
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}



	@Override
	public String toString() {
		return "Product [id=" + idOrder + ", idClient=" + idClient + ", idProduct=" + idProduct + "]";
	}

}