package bll;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;
public class OrderBLL {

	private static ArrayList<Product> prod=new ArrayList();
	private static int total=0;
	public static String receip="";
	static Order newOrder;
	public OrderBLL() {}
	
	public static void addProduct(int idProduct,int q){
		Product pr=ProductDAO.findById(idProduct);//ca sa vad ce produs am
		if(q<=pr.getStock()){
			prod.add(pr);
			total+=pr.getPrice()*q;
			receip+=""+pr.getName()+" "+q+"x"+pr.getPrice()+"\r\n";
			OrderDAO.insert(newOrder);
			ProductDAO.update(pr);}
		else
			System.out.println("Not enough products in stock");
}
public static String getReceip(){
	receip+="Total price :"+total;
	try {
	    PrintWriter writer = new PrintWriter("receip.txt", "UTF-8");
	    writer.println(receip);
	    writer.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return receip;
}
public static void makeOrder(int idOrder,int idClient, int idProduct){
	// TODO Auto-generated method stub
	newOrder=new Order(idClient,idProduct,idOrder);
	newOrder.setIdClient(idClient);
	newOrder.setIdProduct(idProduct);
	newOrder.setIdOrder(idOrder);
	OrderDAO.insert(newOrder);
	
}
}
		