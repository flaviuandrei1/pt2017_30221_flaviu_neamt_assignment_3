package bll;


import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;
public class ClientBLL {

	private static ArrayList<Validator<Client>> validators;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
	}

	public Client findClientById(int id) {
		Client st = ClientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return st;
	}

	public static int insertClient(Client client) {

			return ClientDAO.insert(client);
	}
	public static void deleteClient(Client client){
		ClientDAO.delete(client);
	}
	public static void updateClient(Client client){
		ClientDAO.update(client);
	}
	public static ArrayList<Client> selectAll(){
		return ClientDAO.selectAll();
	}
	
}