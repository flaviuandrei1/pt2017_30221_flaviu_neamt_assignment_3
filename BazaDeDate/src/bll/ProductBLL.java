package bll;


import java.util.ArrayList;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;
public class ProductBLL {
	public Product findProductById(int idProduct){
		Product p=ProductDAO.findById(idProduct);
		if(p==null){
			throw new NoSuchElementException("The Product with id =" + idProduct + " was not found!");
			
		}
		return p;
	}

	public static int insertProduct(Product Product){
			return ProductDAO.insert(Product); 
			
		}
	public ArrayList<Product> selectAllProducts(){
		return ProductDAO.selectAll();
	}
	public static void deleteProduct(int id) {
		Product pr=ProductDAO.findById(id);
		ProductDAO.delete(pr);
	}
	
	public static void updateProduct(Product prd) {
		ProductDAO.update(prd);
	}

}
