package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interfatu {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfatu window = new Interfatu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfatu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnClients = new JButton("Clients");
		btnClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnClients.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ClientInterfatu sup=new ClientInterfatu();
				sup.setVisible(true);
			}
		});
		btnClients.setBounds(10, 25, 208, 59);
		frame.getContentPane().add(btnClients);
		
		JButton btnProducts = new JButton("Products");
		btnProducts.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProductInterfatu p=new ProductInterfatu();
				p.setVisible(true);
			}
		});
		btnProducts.setBounds(10, 95, 208, 58);
		frame.getContentPane().add(btnProducts);
		
		JButton btnOrders = new JButton("Orders");
		btnOrders.addMouseListener(new MouseAdapter() {
			
		@Override
			public void mouseClicked(MouseEvent e) {
			OrderInterfatu o=new OrderInterfatu();
			o.setVisible(true);
			}
		});
		btnOrders.setBounds(10, 164, 208, 58);
		frame.getContentPane().add(btnOrders);
	}
}
