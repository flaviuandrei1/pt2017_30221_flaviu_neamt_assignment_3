package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.ClientBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import model.Client;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class ClientInterfatu extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientInterfatu frame = new ClientInterfatu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClientInterfatu() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Insert");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				InsertClient cli=new InsertClient();
				cli.setVisible(true);
			}
		});
		
		btnNewButton.setBounds(169, 39, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Upgrade");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				UpdateClient clu=new UpdateClient();
				clu.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(169, 73, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Delete");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int idu=Integer.parseInt(textField.getText());
				System.out.println("aici");
				Client tutu=ClientDAO.findById(idu);
			//	System.out.println(tutu.getId());
				ClientBLL.deleteClient(tutu);
			}
		});
		btnNewButton_2.setBounds(124, 131, 89, 23);
		contentPane.add(btnNewButton_2);
		
		textField = new JTextField();
		textField.setBounds(216, 132, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnShowAll = new JButton("Show All");
		btnShowAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ShowAllInterfatu show =new ShowAllInterfatu();
				show.setVisible(true);
			}
		});
		
		btnShowAll.setBounds(169, 107, 89, 23);
		contentPane.add(btnShowAll);
	}
}
