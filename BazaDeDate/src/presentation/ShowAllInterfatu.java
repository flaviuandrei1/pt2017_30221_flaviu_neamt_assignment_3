package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.ClientDAO;
import model.Client;
import model.Product;
import javax.swing.JTable;

public class ShowAllInterfatu extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowAllInterfatu frame = new ShowAllInterfatu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowAllInterfatu() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		ArrayList<Client> clients =ClientDAO.selectAll();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		String[] cols={"Client id","Name","Address","E-mail"};
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("Person id");
		model.addColumn("Name");
		model.addColumn("Address");
		model.addColumn("E-mail");
		model.addRow(cols);
		
	
		for(Client p:clients){
			Object[] o={p.getId(),p.getName(),p.getAddress(),p.getEmail()};
			model.addRow(o);
			}
		
		table = new JTable(model);
		table.setBounds(10, 1,400 , 150);
		contentPane.add(table);
		
	}
}
