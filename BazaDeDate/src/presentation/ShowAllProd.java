package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dao.ClientDAO;
import dao.ProductDAO;
import model.Client;
import model.Product;
import javax.swing.JTable;

public class ShowAllProd extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowAllProd frame = new ShowAllProd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowAllProd() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		ArrayList<Product> products =ProductDAO.selectAll();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		String[] cols={"Product id","Name","Price","Stock"};
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("Person id");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addColumn("Stock");
		model.addRow(cols);
		
	
		for(Product p:products){
			Object[] o={p.getId(),p.getName(),p.getPrice(),p.getStock()};
			model.addRow(o);
			}
		
		table = new JTable(model);
		table.setBounds(10, 1,400 , 150);
		contentPane.add(table);
		
	}
}
