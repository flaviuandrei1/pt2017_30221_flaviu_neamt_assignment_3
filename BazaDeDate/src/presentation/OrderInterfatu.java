package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.OrderBLL;
import dao.OrderDAO;
import model.Order;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JLabel;

public class OrderInterfatu extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblIdclient;
	private JLabel lblIdproduct;
	private JLabel lblQuantity;
	private JButton btnAdd;
	private JButton btnGetReceip;
	private JButton btnCreateNewOrder;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderInterfatu frame = new OrderInterfatu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OrderInterfatu() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(97, 33, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(97, 64, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(97, 100, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		lblIdclient = new JLabel("IDClient");
		lblIdclient.setBounds(20, 36, 46, 14);
		contentPane.add(lblIdclient);
		
		lblIdproduct = new JLabel("IDProduct");
		lblIdproduct.setBounds(20, 67, 67, 14);
		contentPane.add(lblIdproduct);
		
		lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(20, 103, 46, 14);
		contentPane.add(lblQuantity);
		
		btnAdd = new JButton("ADD");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int quanti=Integer.parseInt(textField_2.getText());
				int idprod=Integer.parseInt(textField_1.getText());
				OrderBLL.addProduct(idprod, quanti);
			}
		});
		btnAdd.setBounds(219, 63, 89, 23);
		contentPane.add(btnAdd);
		
		btnGetReceip = new JButton("Get Receip");
		btnGetReceip.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				OrderBLL.getReceip();
			}
		});
		btnGetReceip.setBounds(127, 145, 89, 23);
		contentPane.add(btnGetReceip);
		
		btnCreateNewOrder = new JButton("Create New Order");
		btnCreateNewOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int idord=Integer.parseInt(textField_3.getText());
				int idclie=Integer.parseInt(textField.getText());
				int idprod=Integer.parseInt(textField_1.getText());
				OrderBLL.makeOrder(idclie, idprod,idord);
			}
		});
		btnCreateNewOrder.setBounds(97, 197, 145, 23);
		contentPane.add(btnCreateNewOrder);
		
		textField_3 = new JTextField();
		textField_3.setBounds(130, 230, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
	}
}
