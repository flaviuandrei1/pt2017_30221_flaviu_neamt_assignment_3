package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Product;

public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Product (idProduct,Name,Stock,Price)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM Product where idProduct = ?;";
	private static final String deleteStatementString = "DELETE FROM Product where idProduct=?";
	private final static String updateStatementString = "UPDATE Product SET Name=?, Price=?, Stock=? WHERE idProduct=?";
	private final static String selectallStatementString = "SELECT * FROM Product;";
	public static Product findById(int idProduct) {
		Product toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idProduct);
			rs = findStatement.executeQuery();
			rs.next();
			String name = rs.getString("Name");
			int price = rs.getInt("Price");
			int stock = rs.getInt("Stock");
			
			
			toReturn = new Product(idProduct,name,price,stock);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Product Product){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, Product.getId());
			insertStatement.setString(2, Product.getName());
			insertStatement.setInt(3, Product.getPrice());
			insertStatement.setInt(4, Product.getStock());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void delete(Product product){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, product.getId());
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static void update(Product Product){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			
			updateStatement.setString(1, Product.getName());
			updateStatement.setInt(2, Product.getPrice());
			updateStatement.setInt(3, Product.getStock());
			updateStatement.setInt(4, Product.getId());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<Product> selectAll() {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectAllStatement = null;
		ResultSet rs = null;
		ArrayList<Product> Products = new ArrayList<Product>();
		
		try {
			selectAllStatement = dbConnection.prepareStatement(selectallStatementString);
			rs = selectAllStatement.executeQuery();
			while (rs.next()) {
				int idProduct = rs.getInt("idProduct");
				int price = rs.getInt("Price");
				int stock = rs.getInt("Stock");
				String name = rs.getString("Name");
				Products.add(new Product(idProduct, name,price, stock));
			}	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:select" + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectAllStatement);
			ConnectionFactory.close(dbConnection);
		} 
		
		return Products;
	}

}