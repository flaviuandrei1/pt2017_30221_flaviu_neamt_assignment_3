package dao;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Client (idClient,Name,Address,mail)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM Client where idClient = ?";
	private static final String deleteStatementString = "DELETE FROM client where idClient=?";
	private final static String updateStatementString = "UPDATE Client SET name= ?,address=?, mail= ? where idClient=?;";
	private final static String selectallStatementString = "SELECT * FROM Client;";
	public static Client findById(int clientId) {
		Client toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("Name");
			String address = rs.getString("Address");
			String email = rs.getString("mail");
			
			toReturn = new Client(clientId,name,address,email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getId());
			insertStatement.setString(2, client.getName());
			insertStatement.setString(3, client.getAddress());
			insertStatement.setString(4, client.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void delete(Client client){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, client.getId());
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static void update(Client client){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, client.getName());
			updateStatement.setString(2, client.getAddress());
			updateStatement.setString(3, client.getEmail());
			updateStatement.setInt(4, client.getId());
	
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<Client> selectAll() {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectAllStatement = null;
		ResultSet rs = null;
		ArrayList<Client> clients = new ArrayList<Client>();
		
		try {
			selectAllStatement = dbConnection.prepareStatement(selectallStatementString);
			rs = selectAllStatement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("idClient");
				String name = rs.getString("Name");
				String email = rs.getString("Address");
				String phoneNumber = rs.getString("mail");
				clients.add(new Client(id, name, email, phoneNumber));
			}	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:select" + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectAllStatement);
			ConnectionFactory.close(dbConnection);
		} 
		
		return clients;
	}

}